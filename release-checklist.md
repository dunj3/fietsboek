# Checklist for New Releases

- [ ] Update the version
	- [ ] `pyproject.toml`
	- [ ] `CHANGELOG.rst`
- [ ] Generate an update script: `fietsupdate revision -c development.ini vX.Y.Z`
- [ ] Commit those changes (`git add ... && git commit -m 'bump version to X.Y.Z'`)
- [ ] Make sure the directory is clean (no uncommited changes)
- [ ] Make sure the tests & lints pass
	- [ ] Make sure they also do so on **Python 3.10** (current minimum version)
- [ ] Create a new git tag: `git tag -a vX.Y.Z`
- [ ] Push the tag to the remote repositories
- [ ] Publish to PyPI: `poetry publish`
