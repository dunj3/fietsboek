FROM python:3.11
RUN pip install gunicorn psycopg2-binary mysqlclient

RUN mkdir /package
WORKDIR /package/
COPY ["fietsboek", "fietsboek"]
COPY ["pyproject.toml", "README.md", "LICENSE.txt", "CHANGELOG.rst", "production.ini", "."]
RUN pip install .

COPY --chmod=755 ["container/entrypoint", "/bin/entrypoint"]
COPY ["container/gunicorn.conf.py", "/fietsboek/gunicorn.conf.py"]

VOLUME /fietsboek/data /fietsboek/database /fietsboek/pages
WORKDIR /fietsboek

ENTRYPOINT ["/bin/entrypoint"]
EXPOSE 8000
CMD ["gunicorn", "--paste", "/fietsboek/fietsboek.ini"]
