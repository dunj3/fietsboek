_default:
    @just --list

# Create a new language pack
create-language-pack locale:
    #!/bin/bash
    set -euo pipefail
    [ -e "language-packs/fietsboek-i18n-{{ locale }}" ] && (echo "Already exists!" ; exit 1)
    FB_PATH="$PWD"
    mkdir -p "language-packs/fietsboek-i18n-{{ locale }}"
    cd "language-packs/fietsboek-i18n-{{ locale }}"
    {
        echo '[tool.poetry]'
        echo 'name = "fietsboek-i18n-{{ locale }}"'
        echo 'version = "0.1.0"'
        echo 'description = ""'
        echo 'authors = ["John Doe <john@example.com>"]'
        echo 'packages = [{include = "fietsboek_i18n_{{ locale }}"}]'
        echo '[tool.poetry.dependencies]'
        echo 'python = "^3.7"'
        echo '[build-system]'
        echo 'requires = ["poetry-core"]'
        echo 'build-backend = "poetry.core.masonry.api"'
    } >> pyproject.toml
    mkdir -p "fietsboek_i18n_{{ locale }}/locale"
    touch "fietsboek_i18n_{{ locale }}/__init__.py"
    cp -r "$FB_PATH/fietsboek/locale/en/html" "fietsboek_i18n_{{ locale }}/locale/{{ locale }}/"
    pybabel init -d "fietsboek_i18n_{{ locale }}/locale" -l {{ locale }} -i "$FB_PATH/fietsboek/locale/fietslog.pot"
    echo "{{ locale }}" > fietsboek_i18n_{{ locale }}/locale/{{ locale }}/DISPLAY_NAME

# Compile the messages of an existing language pack
compile-language-pack locale:
    #!/bin/bash
    set -euo pipefail
    cd "language-packs/fietsboek-i18n-{{ locale }}"
    pybabel compile -d "fietsboek_i18n_{{ locale }}/locale" -l {{ locale }} -i "fietsboek_i18n_{{ locale }}/locale//{{ locale }}/LC_MESSAGES/messages.po"

# Update the messages contained in the given pack
update-language-pack locale:
    #!/bin/bash
    set -euo pipefail
    FB_PATH="$PWD"
    cd "language-packs/fietsboek-i18n-{{ locale }}"
    pybabel update -d "fietsboek_i18n_{{ locale }}/locale" -l {{ locale }} -i "$FB_PATH/fietsboek/locale/fietslog.pot"

# Initializes a new built-in language
init-language locale:
    pybabel init -d fietsboek/locale -l {{ locale }} -i fietsboek/locale/fietslog.pot
    cp -r fietsboek/locale/en/html fietsboek/locale/{{ locale }}/
    echo "{{ locale }}" > fietsboek/locale/{{ locale }}/DISPLAY_NAME

# Update the built-in message catalogue
update-language locale:
    pybabel update -d fietsboek/locale -l {{ locale }} -i fietsboek/locale/fietslog.pot

# Compile the given built-in language
compile-language locale:
    pybabel compile -d fietsboek/locale -l {{ locale }} -i fietsboek/locale/{{ locale }}/LC_MESSAGES/messages.po

# Extract new messages from the source files
extract-messages:
    pybabel extract -F babel.cfg -o fietsboek/locale/fietslog.pot --input-dirs=fietsboek

# Copy the assets from the NPM packages
copy-npm-assets:
    # Bootstrap
    cp node_modules/bootstrap/dist/js/bootstrap.bundle.min.js fietsboek/static/bootstrap.bundle.min.js
    cp node_modules/bootstrap/dist/css/bootstrap.css fietsboek/static/bootstrap.css
    # Bootstrap icons
    cp node_modules/bootstrap-icons/font/bootstrap-icons.css fietsboek/static/bootstrap-icons.css
    cp node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff fietsboek/static/fonts/bootstrap-icons.woff
    cp node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff2 fietsboek/static/fonts/bootstrap-icons.woff2
    # Chart.JS
    cp node_modules/chart.js/dist/chart.umd.js fietsboek/static/chart.umd.js
    cp node_modules/chart.js/dist/chart.umd.js.map fietsboek/static/chart.umd.js.map

# Compile the sass assets
compile-sass:
    npx sass asset-sources/theme.scss fietsboek/static/theme.css

# Compile the TypeScript files
compile-typescript:
    npx tsc
