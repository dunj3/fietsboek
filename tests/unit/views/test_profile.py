import pytest
import datetime

from fietsboek.views import profile


@pytest.mark.parametrize("current, prev_month, next_month", [
    ((2024, 2, 1), (2024, 1, 1), (2024, 3, 1)),
    ((2024, 1, 1), (2023, 12, 1), (2024, 2, 1)),
    ((2024, 12, 1), (2024, 11, 1), (2025, 1, 1)),
    ((2024, 5, 5), (2024, 4, 1), (2024, 6, 1)),
    ((2024, 7, 31), (2024, 6, 1), (2024, 8, 1)),
])
def test_prev_next_month(current, prev_month, next_month):
    current = datetime.date(*current)
    prev_month = datetime.date(*prev_month)
    next_month = datetime.date(*next_month)

    actual_prev, actual_next = profile.prev_next_month(current)
    assert actual_prev == prev_month
    assert actual_next == next_month
