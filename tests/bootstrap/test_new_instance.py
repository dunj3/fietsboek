"""Test to ensure that bootstrapping a new instance works with the fietsupdate
script, as described in the documentation.
"""

import contextlib
import logging
import os
import shutil
import subprocess
import venv
from pathlib import Path

LOGGER = logging.getLogger(__name__)
REPO_BASE = Path(__file__).parent.parent.parent


@contextlib.contextmanager
def chdir(path: Path):
    """Change path as context manager.

    Changes back to the previous path on ``__exit__``.
    """
    cwd = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(cwd)


def install_fietsboek(venv_path: Path) -> Path:
    """Creates a virtual env in the given path and installs fietsboek.

    :param venv_path: Path in which the virtual environment should be created.
    :return: The path to the ``bin`` directory in which all scripts reside.
    """
    venv.create(venv_path, with_pip=True)
    subprocess.check_call([venv_path / "bin" / "pip", "install", str(REPO_BASE)])
    return venv_path / "bin"


def create_config(config_name: Path):
    """Copies the testing.ini config and fills in the placeholders.

    :param config_name: Path to the resulting config.
    """
    shutil.copy(REPO_BASE / "testing.ini", config_name)
    config = config_name.read_text()
    config = config.replace("# %% fietsboek.data_dir %%", "fietsboek.data_dir = %(here)s/data")
    config_name.write_text(config)
    Path("data").mkdir()


def test_setup_via_fietsupdate(tmpdir):
    with chdir(tmpdir):
        # We create a new temporary virtual environment with a fresh install, just
        # to be sure there's as little interference as possible.
        LOGGER.info("Installing Fietsboek into clean env")
        binaries_path = install_fietsboek(tmpdir / "venv")

        LOGGER.info("Creating a test configuration")
        create_config(Path("testing.ini"))

        # Try to run the migrations
        subprocess.check_call(
            [binaries_path / "fietsupdate", "update", "-c", "testing.ini", "-f"]
        )

        # Also try to add an administrator
        subprocess.check_call([
            binaries_path / "fietsctl",
            "user",
            "add",
            "-c", "testing.ini",
            "--email", "foobar@example.com",
            "--name", "Foo Bar",
            "--password", "raboof",
            "--admin",
        ])

        assert True
