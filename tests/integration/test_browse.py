import io
import zipfile
from contextlib import contextmanager
from datetime import datetime

from testutils import load_gpx_asset
from fietsboek import models
from fietsboek.models.track import Visibility


@contextmanager
def added_tracks(tm, dbsession, owner, data_manager):
    """Adds some tracks to the database session.

    This function should be used as a context manager and it ensures that the
    added tracks are deleted again after the test, to make a clean slate for
    the next test.
    """
    # The normal transaction is "doomed", so we need to abort it, start a fresh
    # one, and then explicitely commit it, otherwise we will not persist the
    # objects to the database.
    tm.abort()

    tracks = []
    with tm:
        track = models.Track(
            owner=owner,
            title="Foobar",
            visibility=Visibility.PUBLIC,
            description="A foo'd track",
            badges=[],
            link_secret="foobar",
            tagged_people=[],
        )
        track.date = datetime(2022, 3, 14, 9, 26, 54)
        dbsession.add(track)
        dbsession.flush()
        data_manager.initialize(track.id).compress_gpx(load_gpx_asset("MyTourbook_1.gpx.gz"))
        tracks.append(track)

        track = models.Track(
            owner=owner,
            title="Barfoo",
            visibility=Visibility.PUBLIC,
            description="A bar'd track",
            badges=[],
            link_secret="barfoo",
            tagged_people=[],
        )
        track.date = datetime(2022, 10, 29, 13, 37, 11)
        dbsession.add(track)
        dbsession.flush()
        data_manager.initialize(track.id).compress_gpx(load_gpx_asset("Teasi_1.gpx.gz"))
        tracks.append(track)

    tm.begin()
    tm.doom()

    try:
        yield tracks
    finally:
        tm.abort()
        with tm:
            for track in tracks:
                dbsession.delete(track)
        tm.begin()
        tm.doom()


def test_browse(testapp, dbsession, route_path, logged_in, tm, data_manager):
    # pylint: disable=too-many-positional-arguments
    # Ensure there are some tracks in the database
    with added_tracks(tm, dbsession, logged_in, data_manager):
        # Now go to the browse page
        browse = testapp.get(route_path('browse'))

        assert "Foobar" in browse.text
        assert "Barfoo" in browse.text


def test_archive(testapp, dbsession, route_path, logged_in, tm, data_manager):
    # pylint: disable=too-many-positional-arguments
    with added_tracks(tm, dbsession, logged_in, data_manager):
        archive = testapp.get(
            route_path('track-archive', _query=[("track_id[]", "1"), ("track_id[]", "2")])
        )
        result = io.BytesIO(archive.body)

        with zipfile.ZipFile(result, 'r') as zipped:
            assert len(zipped.namelist()) == 2
            assert "track_1.gpx" in zipped.namelist()
            assert "track_2.gpx" in zipped.namelist()
