import re

import pytest

import fietsboek.email
from fietsboek import models

VERIFICATION_LINK_PATTERN = re.compile("http://example.com(/token/[A-Za-z0-9-]+)")


@pytest.fixture
def mailcatcher(monkeypatch):
    """Monkeypatches the send mail functionality.

    Returns the list of mails sent.
    """
    mails = []
    def send_message(_server_url, _username, _passwords, message):
        mails.append(message)
    monkeypatch.setattr(fietsboek.email, "send_message", send_message)
    yield mails


def test_registration_working(testapp, dbsession, route_path, mailcatcher):
    """Ensures that a user can register, including using the verification link."""
    registration = testapp.get(route_path('create-account'))
    form = registration.form
    form['email'] = 'foo-new@bar.com'
    form['name'] = 'The new Foo'
    form['password'] = 'foobarpassword'
    form['repeat-password'] = 'foobarpassword'
    response = form.submit().maybe_follow()

    assert b'A confirmation link has been sent' in response.body
    assert len(mailcatcher) == 1

    user = dbsession.execute(models.User.query_by_email('foo-new@bar.com')).scalar_one()
    assert not user.is_verified

    body = mailcatcher[0].get_body().get_content()
    token_path = VERIFICATION_LINK_PATTERN.search(body).group(1)
    testapp.get(token_path)

    assert user.is_verified


def test_resend_verification_mail(testapp, dbsession, route_path, mailcatcher):
    """Ensures that the verification link re-sending works."""
    registration = testapp.get(route_path('create-account'))
    form = registration.form
    form['email'] = 'foo-new@bar.com'
    form['name'] = 'The new Foo'
    form['password'] = 'foobarpassword'
    form['repeat-password'] = 'foobarpassword'
    form.submit().maybe_follow()

    req = testapp.get(route_path('resend-verification'))
    req.form['email'] = 'foo-new@bar.com'
    req.form.submit().maybe_follow()

    assert len(mailcatcher) == 2
    assert VERIFICATION_LINK_PATTERN.search(mailcatcher[0].get_body().get_content())
    assert VERIFICATION_LINK_PATTERN.search(mailcatcher[1].get_body().get_content())


def test_registration_short_password(testapp, route_path):
    """Ensures that passwords that are too short are rejected."""
    registration = testapp.get(route_path('create-account'))
    form = registration.form
    form['email'] = 'foo-new@bar.com'
    form['name'] = 'The new Foo'
    form['password'] = 'foo'
    form['repeat-password'] = 'foo'
    response = form.submit().maybe_follow()

    assert re.search(
        b'<div class="alert alert-primary" role="alert">\\s*Password not long enough\\s*</div>',
        response.body,
    )


def test_registration_password_mismatch(testapp, route_path):
    """Ensures that passwords that do not match are rejected."""
    registration = testapp.get(route_path('create-account'))
    form = registration.form
    form['email'] = 'foo-new@bar.com'
    form['name'] = 'The new Foo'
    form['password'] = 'foobarfoobar'
    form['repeat-password'] = 'foobarfoo'
    response = form.submit().maybe_follow()

    assert re.search(
        b'<div class="alert alert-primary" role="alert">\\s*'
        b'Passwords don&#39;t match\\s*</div>',
        response.body,
    )
