import re

from sqlalchemy import select, func
from webtest import Upload

from testutils import load_gpx_asset
from fietsboek import models

def test_upload_forbidden(testapp, route_path):
    testapp.get(route_path('upload'), status="4*")


def test_upload(testapp, dbsession, route_path, logged_in):
    # Step 1: Go to the upload form
    upload_site = testapp.get(route_path('upload'))

    # Step 2: Fill it out
    upload_form = upload_site.form
    upload_form['gpx'] = Upload('super_track.gpx', load_gpx_asset('Teasi_1.gpx.gz'))
    result = upload_form.submit().maybe_follow()

    # Step 3: We are now on the "Finish upload" page
    assert "<h1>Upload</h1>" in result.text

    finish_form = result.forms[0]
    finish_form["title"].value = "FoOoOo Bar's Testtrack"
    finish_form["description"].value = "Super descriptive text!"
    # The tagged-friend[] has a disabled attribute, which webtest does not
    # respect. Therefore, we need to manually remove it:
    finish_form.field_order = [
        (name, field) for (name, field) in finish_form.field_order if name != "tagged-friend[]"
    ]

    result = finish_form.submit().maybe_follow()
    assert re.search("<h1>\\s*FoOoOo Bar&#39;s Testtrack", result.text)

    # Step 4: Ensure the track is stored right
    track = dbsession.execute(select(models.Track)).scalar_one()
    assert track.title == "FoOoOo Bar's Testtrack"
    assert track.description == "Super descriptive text!"


def test_upload_cancel(testapp, dbsession, route_path, logged_in):
    # Step 1: Go to the upload form
    upload_site = testapp.get(route_path('upload'))

    # Step 2: Fill it out
    upload_form = upload_site.form
    upload_form['gpx'] = Upload('super_track.gpx', load_gpx_asset('Teasi_1.gpx.gz'))
    result = upload_form.submit().maybe_follow()

    # Step 3: We are now on the "Finish upload" page
    assert "<h1>Upload</h1>" in result.text

    cancel_form = result.forms['cancelForm']
    result = cancel_form.submit().maybe_follow()
    assert "Upload cancelled" in result.text

    # Step 4: Ensure the track is  deleted again
    # This is a workaround for
    # https://github.com/pylint-dev/pylint/issues/8138, a false positive
    # pylint: disable=not-callable
    uploads = dbsession.execute(select(func.count()).select_from(models.Upload)).scalar()
    assert uploads == 0
