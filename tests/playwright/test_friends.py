import datetime

from playwright.sync_api import Page, expect
from sqlalchemy import select

from fietsboek import models

from .conftest import Helper  # pylint: disable=relative-beyond-top-level


def test_friend_not_found(page: Page, playwright_helper: Helper):
    playwright_helper.login()

    page.goto("/me")

    page.get_by_label("Email of the friend").fill("does-not-exist@example.com")
    page.get_by_role("button", name="Send friend request").click()

    expect(page.locator(".alert")).to_have_text("The friend was not found")


def test_send_friend_request(page: Page, playwright_helper: Helper, dbaccess):
    with dbaccess:
        user = models.User(name="Volker", email="vol@ker.de", is_verified=True)
        user.set_password("raboof")
        dbaccess.add(user)
        dbaccess.flush()
        dbaccess.commit()

    playwright_helper.login()

    page.goto("/me")

    page.get_by_label("Email of the friend").fill("vol@ker.de")
    page.get_by_role("button", name="Send friend request").click()

    john = playwright_helper.john_doe()
    reqs = list(
        dbaccess.execute(select(models.FriendRequest).filter_by(sender_id=john.id)).scalars()
    )
    assert len(reqs) == 1
    assert reqs[0].recipient.name == "Volker"


def test_accept_friend_request(page: Page, playwright_helper: Helper, dbaccess):
    john = playwright_helper.john_doe()
    with dbaccess:
        volker = models.User(name="Volker", email="vol@ker.de", is_verified=True)
        volker.set_password("raboof")
        dbaccess.add(volker)
        dbaccess.add(
            models.FriendRequest(sender=john, recipient=volker, date=datetime.datetime.now())
        )
        dbaccess.commit()

    volker = dbaccess.merge(volker)
    john = dbaccess.merge(john)
    playwright_helper.login(volker)

    page.goto("/me")

    page.get_by_role("button", name="Accept").click()

    assert "John Doe" in page.content()

    friends = list(volker.get_friends())
    assert len(friends) == 1
    assert john.id in [friend.id for friend in friends]


def test_accept_by_sending(page: Page, playwright_helper: Helper, dbaccess):
    john = playwright_helper.john_doe()
    with dbaccess:
        volker = models.User(name="Volker", email="vol@ker.de", is_verified=True)
        volker.set_password("raboof")
        dbaccess.add(volker)
        dbaccess.add(
            models.FriendRequest(sender=john, recipient=volker, date=datetime.datetime.now())
        )
        dbaccess.commit()

    volker = dbaccess.merge(volker)
    john = dbaccess.merge(john)
    playwright_helper.login(volker)

    page.goto("/me")

    page.get_by_label("Email of the friend").fill("john@doe.com")
    page.get_by_role("button", name="Send friend request").click()

    assert "John Doe" in page.content()

    friends = list(volker.get_friends())
    assert len(friends) == 1
    assert john.id in [friend.id for friend in friends]


def test_unfriend(page: Page, playwright_helper: Helper, dbaccess):
    john = playwright_helper.john_doe()
    with dbaccess:
        john = dbaccess.merge(john)
        volker = models.User(name="Volker", email="vol@ker.de", is_verified=True)
        volker.set_password("raboof")
        dbaccess.add(volker)
        dbaccess.flush()
        volker.add_friend(john)
        dbaccess.commit()

    volker = dbaccess.merge(volker)
    john = dbaccess.merge(john)
    playwright_helper.login(volker)

    page.goto("/me")
    page.get_by_role("button", name="Unfriend").click()

    friends = list(volker.get_friends())
    assert not friends
