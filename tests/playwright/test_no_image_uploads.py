import pytest
from playwright.sync_api import Page, expect

from testutils import extract_and_upload


@pytest.fixture(scope="module")
def app_settings(app_settings):
    """Override the standard app settings to disable image uploads."""
    app_settings["fietsboek.enable_image_uploads"] = "false"
    return app_settings


def test_image_button_disabled_during_upload(page: Page, playwright_helper, tmp_path, dbaccess):
    playwright_helper.login()

    page.goto("/")
    page.get_by_text("Upload").click()

    # We unpack one of the test GPX files
    extract_and_upload(page, "Teasi_1.gpx.gz", tmp_path)

    # We now fill in most of the data
    expect(page.locator("#selectImagesButton")).to_be_disabled()


def test_image_button_disabled_during_edit(page: Page, playwright_helper, dbaccess):
    playwright_helper.login()
    track_id = playwright_helper.add_track().id

    page.goto(f"/track/{track_id}")
    page.locator(".btn", has_text="Edit").click()

    expect(page.locator("#selectImagesButton")).to_be_disabled()
