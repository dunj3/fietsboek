import time

from playwright.sync_api import Page


def test_tileproxy(page: Page, playwright_helper, caplog):
    playwright_helper.login()
    track_id = playwright_helper.add_track().id

    page.goto(f"/track/{track_id}")

    with page.expect_request("/tile/test/**"):
        pass

    # If we're too fast, the log entry might not be there yet, wait 2 more
    # seconds
    if "Skipping tile proxy request for testing URL" not in caplog.messages:
        time.sleep(2)

    assert "Skipping tile proxy request for testing URL" in caplog.messages
