from playwright.sync_api import Page, expect
from sqlalchemy import select

from fietsboek import models


def test_view_link(page: Page, playwright_helper, dbaccess):
    playwright_helper.login()
    track = playwright_helper.add_track()

    page.goto(f"/track/{track.id}")

    page.locator(".btn", has_text="Share").click()

    expect(page.locator("a", has_text=track.link_secret)).to_be_visible()


def test_view_with_link(page: Page, playwright_helper, dbaccess):
    track = playwright_helper.add_track()

    page.goto(f"/track/{track.id}?secret={track.link_secret}")

    expect(page.locator("h1")).to_have_text("Another awesome track")


def test_view_wrong_link(page: Page, playwright_helper, dbaccess):
    track = playwright_helper.add_track()

    with page.expect_response(lambda resp: resp.status == 403):
        page.goto(f"/track/{track.id}?secret=foobar")

    assert "Forbidden" in page.content()


def test_change_link(page: Page, playwright_helper, dbaccess):
    playwright_helper.login()
    track = playwright_helper.add_track()
    old_secret = track.link_secret

    page.goto(f"/track/{track.id}")

    page.locator(".btn", has_text="Share").click()
    page.get_by_role("button", name="Invalidate link").click()
    page.wait_for_load_state()

    query = select(models.Track).filter_by(id=track.id)
    new_track = dbaccess.execute(query).scalar_one()

    assert old_secret != new_track.link_secret
