from playwright.sync_api import Page, expect


def test_forbidden(page: Page, playwright_helper):
    john = playwright_helper.john_doe()

    with page.expect_response(lambda resp: resp.status == 403):
        page.goto(f"/user/{john.id}")


def test_profile(page: Page, playwright_helper):
    playwright_helper.login()

    page.goto("/")
    page.get_by_role("button", name="User").click()
    page.get_by_role("link", name="Profile").click()

    expect(page.locator("#profileLength")).to_have_text("0 km")
    expect(page.locator("#profileUphill")).to_have_text("0 m")
    expect(page.locator("#profileDownhill")).to_have_text("0 m")
    expect(page.locator("#profileMovingTime")).to_have_text("0:00:00")
    expect(page.locator("#profileStoppedTime")).to_have_text("0:00:00")
    expect(page.locator("#profileMaxSpeed")).to_have_text("0 km/h")
    expect(page.locator("#profileAvgSpeed")).to_have_text("0 km/h")
    expect(page.locator("#profileNumberOfTracks")).to_have_text("0")
