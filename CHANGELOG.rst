Changelog
=========

Unreleased
----------

0.10.0 - 2025-02-05
-------------------

Added
^^^^^

- The count of images and comments of a track in the home page and browse
  views.
- The calendar view in the profile page.
- Per year & per month tooltips.

Changed
^^^^^^^

- Fietsboek now requires at least Python 3.10.
- Fietsboek now uses `Inter <https://rsms.me/inter/>`__ as its font.

Fixed
^^^^^

- Track images not being shown properly on small screens (thanks
  networkjanitor).
- Properly round stopped & movign times to seconds in the track tooltip.

0.9.0 - 2024-04-08
------------------

Added
^^^^^

- The ``fietsctl user modify --set-email`` command.
- The count of tagged people in the home page overview.
- Favourites!
- A button to log out all current sessions.
- A setting to disable image uploads.
- Average track length & duration on profile page.
- The language chooser.
- The first graph on the profile page.

Changed
^^^^^^^

- GPX file downloads now set the filename when downloading.

Fixed
^^^^^

- Friends that are already tagged are hidden when searching for friends to tag.
- Parsing of the custom tile layers.
- Bootstrapping of new instances (thanks networkjanitor).

0.8.0 - 2023-06-05
------------------

Added
^^^^^

- The ability to directly import ``.fit`` files.
- A setting to limit the number of ``hittekaart`` threads.
- A button to change the sorting on the home page.
- Different sortings on the "Browse" page.
- The "Fix elevation jumps" transformer
- The ``fietsctl track del``, ``fietsctl track list`` and ``fietsctl user
  modify`` commands.
- The ability to request a new verification mail.

Changed
^^^^^^^

- The user tiles now return an empty tile instead of a 404 for tiles that have
  no content.
- Images on a track page are sorted by their original file name.
- The "Fietsboek" icon and tag are now a link to the home page.
- Tokens now expire after 24 hours.
- The ``fietsctl`` command names have been changed.

Fixed
^^^^^

- A missing script tag has been added again, which fixes two bugs:
    - The tileproxy not being used.
    - Only the default map layers being shown.
- GPX files with links (like the BRouter ones) are now saved as valid GPX.

0.7.0 - 2023-04-24
------------------

Added
^^^^^

- Profile pages with "milestone tracks" (longest, shortest, ...).
- Integration with ``hittekaart`` for heatmaps on the profile.
- A data version check at startup.
- The *Remove Breaks* transformer.
- Unfinished uploads are now shown to the user on the home page.

Changed
^^^^^^^

- ``fietsctl`` now uses ``click`` for argument parsing, some CLI behavior might
  have changed.
- All commands now assume ``fietsboek.ini`` as the default configuration path.
- `bleach <https://github.com/mozilla/bleach/>`__ has been replaced with `nh3
  <https://pypi.org/project/nh3/>`__.

0.6.0 - 2023-03-08
------------------

Added
^^^^^

- The maintenance mode.
- The summary on the home page now shows the number of tracks per time period.
- The summary on the home page now shows the track length at first glance.
- Transformers 🎉

Changed
^^^^^^^

- Python 3.9 is the new minimum Python version (up from 3.7).
- The tile proxy will now do at most 2 concurrent requests per tile provider.

Fixed
^^^^^

- Page reading for systems that use a non-UTF-8 locale.
- The filename above the map is hidden again.
- Inconsistency issues when exceptions would happen during the upload/editing
  of a track.

0.5.0 - 2023-01-12
------------------

Added
^^^^^

- A "Remember me" option when logging in.
- The ability to load external (third party) language packs.
- GPX metadata (track title, description & author) are now embedded into the
  actual GPX file.
- The ``fietscron`` maintenance script.

Changed
^^^^^^^

- The configuration file is now parsed and validated at application startup
  with better error reports.
- GPX content is now delivered compressed if the browser supports it.
- GPX files are now stored outside of the database.

Fixed
^^^^^

- Account registration giving a 400 error.
- Track deletion being forbidden for users.
- Descriptions embedded in GPX tracks not being pre-filled when
  uploading the track.
- Maximum zoom levels for the map not being respected.

0.4.0 - 2022-11-28
------------------

Added
^^^^^

- Support to render pages with custom (static) content.
- Support to overwrite the home page's content.
- Autocompletion for tags in upload & edit forms.
- The ``fietsupdate`` script for arbitrary update commands.

Changed
^^^^^^^

- Filters are now evaluated in SQL for better efficiency.
- Tiles are now proxied through Fietsboek.

Fixed
^^^^^

- Track participants not being shown anymore.

0.3.0 - 2022-08-12
------------------

Added
^^^^^

- Buttons to collapse months/years in the personal summary.
- Filters to search for tracks in the browse view.
- The ability to upload "track templates", i.e. pre-planned routes without a
  "proper" recording.
- The ability to use a `Thunderforest <https://www.thunderforest.com/>`__ API
  key.

Fixed
^^^^^

- Images not saving when added during the track upload.
- Tracks without time information crashing the upload.

0.2.1 - 2022-07-24
------------------

Added
^^^^^

- A fullscreen button to the map.

Changed
^^^^^^^

- The behaviour of pressing enter in the "Add Tag" and "Search Friend" fields.

Fixed
^^^^^

- Images not showing when using the track's share link.

0.2.0 - 2022-07-23
------------------

Added
^^^^^

- A button to delete tracks.
- A small "user menu" in the top right corner.
- A button to download multiple tracks in the "Browse" view.
- Image uploading.

Changed
^^^^^^^

- The position of the login/logout buttons.
- Style of the "Tag friend" button.

Fixed
^^^^^

- The track editing overwriting the timezone information.
- The manifest file for inclusion of the README and CHANGELOG.

0.1.0 - 2022-07-09
------------------

Added
^^^^^

- Allow image elements in markdown descriptions.
- Send caching headers for GPX and badge files.
- Take title suggestion from GPX tracks.
- CSRF protection for actions that modify data.

Fixed
^^^^^

- Ensure better caching of the track metadata.
- Fix style for the `<strong>` element.
- Update styles for password reset forms.
- Better handling for timezones in GPX files.

0.0
---

- Initial version.
