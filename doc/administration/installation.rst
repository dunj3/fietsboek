Installation
============

This document will outline the installation process of Fietsboek, step-by-step.

Requirements
------------

Fietsboek has the following requirements (apart from the Python modules, which
will be installed by ``pip``):

* Python 3.10 or later
* A `redis <https://redis.com/>`__ server, used for caching and temporary data
* (Optionally) an SQL database server like `PostgreSQL
  <https://www.postgresql.org/>`__ or `MariaDB <https://mariadb.org/>`__ (if
  SQLite is not enough)

In addition, if you run on a different interpreter than CPython, you might need
a working Rust toolchain (``rustc`` and ``cargo``) installed. This is because
some of Fietsboek's dependencies (such as ``pydantic`` and ``nh3``) use Rust
modules. If they don't provide a binary wheel for your interpreter, they will
automatically build one during the installation process — but that requires the
Rust toolchain.

To install Rust, check out your distributions wiki/packages, or use the `rustup
tool`_.

.. _rustup tool: https://rustup.rs/

Creating an Environment
-----------------------

It is advised that you keep your Fietsboek installation in a separate `virtual
environment <https://virtualenv.pypa.io/en/latest/>`__. This makes sure that
the dependencies that Fietsboek pulls in do not interfere with your system's
libraries, and it makes sure that you can upgrade those components easily
without affecting other installed programs.

We will assume that you create your environment in the ``.venv`` folder. Any
path can be choosen, you just need to remember it for later

.. code:: bash

    # If Python 3 is the default on your system:
    virtualenv .venv
    # Otherwise:
    virtualenv -p python3 .venv

Installing the Python Modules
-----------------------------

The Python package manager takes care of installing all dependencies, all you
need to do is tell it to install Fietsboek. How exactly depends on your way of
acquiring Fietsboek.

Via PIP
^^^^^^^

Fietsboek is available in the Python Package Index as `fietsboek
<https://pypi.org/project/fietsboek/>`__ and can be installed directly via
``pip``:

.. code:: bash

    .venv/bin/pip install fietsboek

Via Git
^^^^^^^

If you prefer to work with the source version (e.g. for development purposes),
you should use Git to clone the repository from one of its mirrors:

* Main on Gitlab: https://gitlab.com/dunj3/fietsboek
* Mirror on Codeberg: https://codeberg.org/dunj3/fietsboek

.. code:: bash

    git clone https://gitlab.com/dunj3/fietsboek
    .venv/bin/pip install ./fietsboek
    # For development, you can use -e for an editable installation:
    .venv/bin/pip install -e ./fietsboek

Optional: Install lxml
^^^^^^^^^^^^^^^^^^^^^^

In any case, you can optionally install ``lxml``, which provides a faster XML
parser to process the GPX files:

.. code:: bash

    .venv/bin/pip install lxml

.. note::

    It is currently unclear whether ``lxml`` does provide a speed benefit over
    the alternative, especially when running on PyPy. See also `issue #7`_.

    .. _issue #7: https://gitlab.com/dunj3/fietsboek/-/issues/7

Configuring Fietsboek
---------------------

Before you can run Fietsboek, you need to adjust your configuration. See
:doc:`configuration` for that.

Setting up the Database
-----------------------

Fietsboek users the ``fietsupdate`` script to handle database migrations and
other update tasks. You can use it to set up the initial database schema:

.. code:: bash

    .venv/bin/fietsupdate update -c production.ini

.. note::

    If you install Fietsboek via Git, and you stick to the ``master`` branch
    instead of a specific version, you must also run ``.venv/bin/alembic -c
    production.ini upgrade head``.

    See :doc:`../developer/updater` ("Furhter notes") for more information.

Adding an Administrator User
----------------------------

You can use the ``fietsctl`` command line program to add administrator users:

.. code:: bash

    .venv/bin/fietsctl user add -c production.ini --admin

Running Fietsboek
-----------------

To run Fietsboek, simply run

.. code:: bash

    .venv/bin/pserve production.ini

.. warning::

    It is strongly advised that you use a httpd configured with SSL as a
    reverse proxy (such as `Apache2
    <https://httpd.apache.org/docs/current/howto/reverse_proxy.html>`__ or
    `nginx <https://nginx.org/en/docs/http/ngx_http_proxy_module.html>`__) to
    handle external traffic to Fietsboek!

    Fietsboek itself does not use SSL encryption for its traffic, and not
    setting up a proper reverse proxy will put your users' data at risk!

The default server uses `waitress
<https://docs.pylonsproject.org/projects/waitress/en/stable/index.html>`__ and
should be enough for small to medium traffic. It is implemented purely in
Python and should run on most systems.

Alternatively, you can switch to `gunicorn <https://gunicorn.org/>`__ or
`mod_wsgi <https://modwsgi.readthedocs.io/en/master/>`__ for better
performance. For those, refer to the Pyramid documentation for more information:

* `gunicorn
  <https://docs.pylonsproject.org/projects/pyramid_cookbook/en/latest/deployment/gunicorn.html>`__
* `mod_wsgi
  <https://docs.pylonsproject.org/projects/pyramid/en/latest/tutorials/modwsgi/index.html>`__

Maintaining Fietsboek
---------------------

A good installation should be well-oiled, which means it should be
:doc:`updated <upgrading>` to new versions and :doc:`backups <backup>` should
be done frequently.
