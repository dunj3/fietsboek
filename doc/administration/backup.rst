Backup & Restore
================

A well-maintained Fietsboek instance should have regular backups configured in
case data is lost due unforseen reasons.

.. note::

    As a rule of thumb: A backup that you haven't restored is not a backup!

    Part of the backup process is to ensure that the backups actually contain
    the necessary data and that the restoration process works. Make sure to
    test your backups on a separate installation of Fietsboek.

Backup
------

The Fietsboek data lives in two places: Track and account metadata is saved in
the database, while GPX files, images and other "big files" are saved in the
data directory.

You should refer to the manual of your DBMS to see what the procedure for a
backup is. Using the preferred way for your DBMS ensures that your backup
represents a consistent state of the database.

.. note::

    Check :doc:`maintenance-mode` to see how the maintenance mode can be
    enabled, which should disable most database accesses during the process.

After backing up the database, you should back up the data directory with a
tool of your choice, for example by using ``tar``, ``rsync`` or a proper backup
tool like ``borg``:

.. code:: bash

    tar -czf backup.tar.gz path/to/data/dir

In addition to the actual data, you should also note down the Fietsboek version
and the database schema version, as backups can only be restored to the same
version!

To view the Fietsboek version in case you are using ``git``, you can use the
following command:

.. code:: bash

    git rev-parse HEAD

If you installed Fietsboek via ``pip`` or other means, you can use one of the
following to see the version:

.. code:: bash

    .venv/bin/pip show fietsboek
    .venv/bin/fietsctl version

To view the data version, use ``fietsupdate``:

.. code:: bash

    .venv/bin/fietsupdate status -c development.ini

To view the database schema version, use ``alembic``:

.. code:: bash

    .venv/bin/alembic -c development.ini current

Note those values (Fietsboek version, data version, database schema version) in
addition to your backup, as you will need them when restoring data later or
when troubleshooting the restoration process.

Restore
-------

The restoration process works in four steps:

First, we ensure that we are on the correct Fietsboek version. If you are using
``git``, this can be done with ``git checkout`` before installing it:

.. code:: bash

    git checkout NOTED_GIT_VERSION_HERE

If you have installed Fietsboek via ``pip``, you can use it to request a
specific version:

.. code:: bash

    .venv/bin/pip install "fietsboek==X.Y.Z"

Next, we run the data migrations:

.. code:: bash

    .venv/bin/fietsupdate update -c development.ini VERSION_FROM_EARLIER

We can verify that the database has the correct schema version by using the
same command from earlier and comparing its output to the noted value:

.. code:: bash

    .venv/bin/alembic -c development.ini current

Now, we can restore the data in the database. This step is dependent on the
DBMS that you, therefore you should consult its documentation.

Finally, we can restore the data directory. This step depends on how you chose
to back up the data directory earlier. In the case of ``tar``, you can simply
extract the archive to the right location.
