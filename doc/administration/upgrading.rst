Upgrading
=========

Fietsboek does not currently have an automated updater, however it is quite
simple to update Fietsboek manually.

.. warning::

    Make sure to have backups of your user data ready in case the upgrade fails!

.. note::

    It is advised to stop the Fietsboek server during the backup process.

Pulling the New Code
--------------------

First, we need to pull the new code. This step depends on the source that you
have installed Fietsboek from. In the case that you use ``git``, this can be
done in the following way:

.. code:: bash

    git pull
    git checkout v42.0  # Put in the version that you want to update to here

If you have downloaded an archive with the Fietsboek source, you need to
download the archive with the new version and extract it somewhere.

.. tip::

    If upgrading fails, try going in smaller steps, e.g. release by release. If
    you upgrade over a big version span, the chances are bigger that unintended
    interactions occur.

    To do that, use ``git`` or ``pip`` to download specific versions of the
    code instead of going for the latest one.

Installing the Python Module
----------------------------

Use ``pip`` to install the new version of the Fietsboek package, as well as any
new additional dependencies:

.. code:: bash

    .venv/bin/pip install path/to/fietsboek/source

If you downloaded Fietsboek from PyPI, you may need to tell ``pip`` explicitely
to upgrade. Otherwise it will simply say that the package is already installed:

.. code:: bash

    .venv/bin/pip install --upgrade fietsboek

In general, this step is similar to the command that you used when installing
Fietsboek initially.

Note that removed dependencies are not automatically removed. It might be worth
to start with a clean virtual environment from time to time to clear out old
cruft.

Upgrading the Database
----------------------

.. warning::

   It cannot be stressed enough that you should have backups of your data. This
   is your last chance.

Some updates might change the database schema. Those updates come with database
migrations that can adapt an existing database to the new schema. In addition
to database migrations, some updates also modify the on-disk data that
Fietsboek stores.

Fietsboek comes with a handy tool that allows you to run the right database and
file migrations: ``fietsupdate``.

You can use the following command to upgrade to the latest installed version:

.. code:: bash

    .venv/bin/fietsupdate update -c production.ini

Or you can view the status of your installation:

.. code:: bash

    .venv/bin/fietsupdate status -c production.ini

.. note::

    The ``fietsupdate`` tool is only there to run database migrations and other
    update tasks. It does *not* retrieve or install the correct code, please
    see the above sections to do that.

Restarting Fietsboek
--------------------

You can now run Fietsboek again. This step depends on the method that you use
to deploy Fietsboek.

Downgrading
===========

In some cases, it might be required that you uninstall an update to Fietsboek,
for example if it introduced a bug. Downgrading is possible and generally
consists of two steps:

1) Restore the old data format (backwards migration)
2) Restore the old code

.. warning::

    As with updates, make sure you have backups of your important data ready!

Restoring Data: Backups
-----------------------

The preferred way to use the old data is by restoring a backup, see
:doc:`backup`. This method ensures that your data is original and prevents the
upgrade/downgrade process from inducing errors.

Restoring Data: Downgrading
---------------------------

If you do not have old backups of your data, you can use ``fietsupdate`` to
convert your data from the new format to the old one. Please note that the
upgrade process can not always be undone: If an update deletes data, even the
downgrade cannot undo it. In such cases, the only way to restore the data is
through a backup.

To use ``fietsupdate`` to downgrade your data, run the following command,
replacing ``VERSION`` with the version you want to downgrade to:

.. code:: bash

    .venv/bin/fietsupdate downgrade -c production.ini VERSION

Restoring Code
--------------

Now that the data has been restored to the old state, you need to restore the
Fietsboek code to the old version. This works similarly to the update, with the
difference that you should download and install the version of Fietsboek that
you downgraded your data to.

If you use ``git``, you can check out old versions of the code by using ``git
checkout``.

After obtaining the old code, don't forget to install it into your virtual
environment!
