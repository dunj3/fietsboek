Manpages
========

.. toctree::
    :maxdepth: 1

    man/fietsctl

In this section, you will find the manpages for the various commands that are
provided in Fietsboek.

Each of those pages can also be turned into a "proper" manpage using
``rst2man``::

    rst2man.py doc/man/fietsctl.rst fietsctl.1
