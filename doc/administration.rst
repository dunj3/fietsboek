Administration Guide
====================

.. toctree::
    :maxdepth: 2
    :caption: Contents

    administration/installation
    administration/configuration
    administration/upgrading
    administration/backup
    administration/cronjobs
    administration/custom-pages
    administration/maintenance-mode
    administration/docker

This guide contains information pertaining to the administration of a Fietsboek
instance. This includes the installation, updating, configuration, and backing
up of Fietsboek.
