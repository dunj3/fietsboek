Developer Guide
===============

.. toctree::
    :maxdepth: 2
    :caption: Contents

    developer/localize
    developer/language-pack
    developer/js-css
    developer/authentication
    developer/updater
    Python Packages <developer/module/modules>

This guide contains information for developers that want to modify or extend
Fietsboek. This includes information on how to localize Fietsboek to new
languages.
