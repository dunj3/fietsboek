JavaScript & CSS
================

Like any web application, Fietsboek makes use of CSS and JavaScript. While
Fietsboek is a Python application, and its dependencies are managed via
Poetry_, it still uses npm_ to download and update the client-side JavaScript
libraries.

Furthermore, Fietsboek uses SASS_ to pre-compile the CSS files, and in the
future will also use TypeScript_ to pre-compile the JavaScript files.
Therefore, it is useful to know at least the basics of the
``sass``/``tsc``/``npm`` utilities.

In any case, all generated files are commited to the repository. As such, if
you don't want to change the JavaScript/CSS files, you do not have to bother
with any of that.

.. warning::

    If you do want to change JavaScript or CSS, always edit the source files.
    Otherwise your changes will get overwritten the next time the
    SASS/TypeScript compiler is run.

.. _Poetry: https://python-poetry.org/
.. _npm: https://www.npmjs.com/
.. _SASS: https://sass-lang.com/
.. _TypeScript: https://www.typescriptlang.org/

Updating JavaScript libraries
-----------------------------

As mentioned, ``npm`` is used to manage the JavaScript libraries. You can check
whether libraries are outdated and update them with the following commands::

    npm outdated
    npm update

In addition, there is a ``justfile`` target that copies the assets from the
:file:`node_modules` directory to the :file:`fietsboek/static/` directory::

    just copy-npm-assets

Compiling SASS
--------------

The SASS style files are located in :file:`asset-sources`. Do *not* edit the
CSS files in :file:`fietsboek/static` directly, as those changes will get
overwritten!

The SASS compiler is installed via ``npm``, and a ``justfile`` target exists to
compile the right SASS files::

    npm install
    just compile-sass

Compiling TypeScript
--------------------

The TypeScript source is located in :file:`asset-sources`. The
:file:`tsconfig.json` contains the necessary settings for the TypeScript
compiler, as such you can simply run ``tsc`` to compile the sources:

.. code-block:: bash

    npx tsc
    # Alternatively, keep watching for file changes:
    npx tsc -w

To complement the ``just compile-sass`` command, a ``just compile-typescript``
command also exists as an alias for ``npx tsc``.

When changing the TypeScript code, you should also run ``eslint``::

    npx eslint asset-sources/fietsboek.ts
