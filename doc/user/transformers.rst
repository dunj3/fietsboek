Transformers
============

Transformers are small (or big) transformations that are applied to your GPX
track after it has been uploaded. This allows Fietsboek to provide some common,
simple editing options, without users having to do that themselves.

All transformers are optional and disabled by default. You can enable
transformers when uploading a track on the bottom of the page. You can also
enable transformers for already uploaded tracks in the editing view.

.. note::

    When enabling transformers for already existing tracks, your browser might
    not show the changed track. Try force-refreshing (Crtl + F5) the page so
    that it reloads the GPX from the server.

In other applications, transformers are sometimes called "filters". That term
however has many different meanings (like the filters on the "Browse" page),
and as such, Fietsboek calls them transformers.

Keep in mind that the transformers provide a "quick and convenient" way to
apply a predefined set of changes to a track. If you need to do fine-grained
edits to a GPX file, you need to use a different tool and edit the file before
uploading it to Fietsboek.

Fix Null Elevation
------------------

The *fix null elevation* transformer removes points at the start and end of a
track that have a "wrong" elevation. This helps to avoid issues when GPX
trackers don't have elevation data yet and fill in 0, leading to wrong uphill
calculations and wrong diagrams:

.. image:: images/wrong_elevation.png
    :width: 200
    :alt: An elevation graph that starts at 0 and makes a jump to 165.

Activating the transformer will produce the following track:

.. image:: images/fixed_elevation.png
    :width: 200
    :alt: The same track, but with a fixed elevation graph that starts at 165.

The transformer considers "wrong" elevation to be either points that have an
elevation of 0, or points that have a slope of more than 100% to the next
point.

To fix those points, the transformer will find the first correct point, and
copy its elevation to the wrong points.

Fix Elevation Jumps
-------------------

The *fix elevation jumps* transformer eliminates big elevation jumps in the
middle of a track. This is useful to deal with "stitched" GPX files, where the
elevation is consistent within a single track, but the absolute value might not
be correct (e.g. if the device recalibrates):

.. image:: images/wrong_elevation_jump.png
   :width: 600
   :alt: The elevation profile having a big jump in the middle.

In this track, the device was re-calibrated mid-track. The transformer will
adjust the elevation values:

.. image:: images/fixed_elevation_jump.png
   :width: 600
   :alt: The same elevation profile with the jump removed.

The detection of jumps work similarly to the *fix null elevation* transformer,
with the difference that it works in the middle of tracks. It will consider the
earlier points as anchor, and then adjust the later points such that the first
point after the jump has the same elevation as the last point before the jump.

Remove Breaks
-------------

The *remove breaks* transformer removes longer breaks of inactivity by deleting
the points and shifting the following points back in time. This is useful e.g.
if you are waiting at a single spot for a while, and you would like that to be
removed for a cleaner log.

Note that this transformer modifies the track's timings. Therefore, the
recording will end earlier than it did in reality, and the stopped time will be
reduced.

A break is removed if the speed is below 1 kilometer per hour (approx. 0.28
meters per second) for more than 5 minutes.
