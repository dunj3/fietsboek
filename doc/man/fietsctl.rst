fietsctl
========

Control utility for Fietsboek
-----------------------------
:Manual section: 1

SYNPOSIS
********

.. code-block:: text

    fietsctl maintenance-mode
    fietsctl track {del|list}
    fietsctl user {add|del|hittekaart|list|modify|passwd}
    fietsctl version

DESCRIPTION
***********

The ``fietsctl`` script is provided for administrative purposes. It allows you
to manage the state and content of a Fietsboek instance from the command line.

.. warning::

    The ``fietsctl`` script does not do any access checking and does not
    require any logins or passwords. You must use the permissions of your
    system and database server to restrict the access to ``fietsctl`` by
    restricting access to the data stores directly.

Detailed versions of the commands are described below.

Note that most commands expect the path to the configuration file to be given
(e.g. ``-c production.ini``). The default uses ``fietsboek.ini``. This can be
overridden using the ``-c``/``--config`` option.

.. note::

    All commands support the ``--help`` option, which will give you a quick
    overview of how the command works and which options are available.

USER MANAGEMENT
***************

You can use the ``fietsctl user`` subcommand to manage the users in the
Fietsboek system.

Many functions which deal with existing users (delete, modify, ...) allow the
user to be specified either by their ID using the ``-i``/``--id`` option, or by
their email address using ``-e``/``--email``. You can obtain the IDs of users
using the ``fietsctl user list`` command.

ADDING A USER
#############

.. code-block:: text

    fietsctl user add [-c CONFIG] [--email EMAIL] [--password PASSWORD] [--admin]

This command adds a user to the system. It can be called with no arguments, in
which case all required values are prompted for.

If the new user should be made an admin, use the ``--admin`` flag. If not
given, the user will *not* be made an admin. In any case, the user is
automatically verified. If you want to change the admin or verification status
after creating a user, use the ``fietsctl user modify`` command (see below).

It is advised that you do not supply the password on the command line to
prevent it from appearing in the command history. Either disable the history,
or leave out the password (in which case you will be prompted).

Note that this function does not check the ``enable_account_registration``
setting, so it can always be used to add new users to the system.

Note further that this function does less checks then the web interface (e.g.
it does not require an email verification), so be careful which values you
enter.

REMOVING A USER
###############

.. code-block:: text

    fietsctl user del [-c CONFIG] [-f/--force] [-i/--id ID] [-e/--email EMAIL]

Removes a user from the system. This will remove the user account and all
associated data (the user's tracks, comments, ...).

By default, the command will ask for confirmation. You can specify the
``-f``/``--force`` flag to override this check.

GENERATING HEATMAPS
###################

.. code-block:: text

    fietsctl user hittekaart [-c CONFIG] [-i/--id ID] [-e/--email EMAIL] [--delete] [--mode heatmap|tilehunt]

With ``fiettsctl user hittekaart`` you can force a hittekaart run for a
specific user. By default, only the heatmap is generated, but you can use
``--mode`` to select which overlay map you want to generate. You can also
specify ``--mode`` multiple times to generate multiple heat maps with a single
invocation.

If you want to delete a heatmap, use the ``--delete`` option. It also respects
the ``--mode`` selection, so you can delete individual types of heatmaps.

LISTING USERS
#############

.. code-block:: text

    fietsctl user list [-c CONFIG]

Outputs a list of all users in the system, one user per line. Each line
consists of:

.. code-block:: text

    [av] ID - EMAIL - NAME

The "a" indicates that the user has admin permissions. If the user has no admin
permissions, a "-" is shown instead.

The "v" indicates that the user has their email address verified. If the user
has not verified their email address, a "-" is shown instead.

MODIFYING USERS
###############

.. code-block:: text

    fietsctl user modify [-c CONFIG] [-i/--id ID] [-e/--email EMAIL] [--admin/--no-admin] [--verified/--no-verified] [--set-email EMAIL]

Modifies a user. This can currently be used to set the admin and verification
status of a user. If you want to change the password, use ``fietsctl user
passwd`` (see below). You cannot currently change the email address or name of
a user with this command (note that the ``--email`` option is for user
*selection*, not *modification*).

If you do not specifiy either ``--admin`` or ``--no-admin``, then the current
value of the user is not changed. The same goes for ``--verified`` and
``--no-verified``, if neither is given, the current value is not changed.

To change a user's email address, you can specify the ``--set-email`` option
with the desired new email address. Note that — just like with adding users —
no email verification is taking place, so be careful when using this. The new
email address is automatically registered as verified.

CHANGING USER PASSWORDS
#######################

.. code-block:: text

    fietsctl user passwd [-c CONFIG] [-i/--id ID] [-e/--email EMAIL] [--password PASSWORD]

Changes the password of the specified user. If the password is not given via
``--password``, then the password is interactively prompted for. Be careful
when using ``--password`` as sensitive information might end up in the shell
history!

Note that this function does fewer checks than the web interface, as such it is
possible to set passwords that users cannot set themselves (e.g. very short
ones).

TRACK MANAGEMENT
****************

The ``fietsctl track`` subcommand can be used to manage the tracks.

LISTING TRACKS
##############

.. code-block:: text

    fietsctl track list [-c CONFIG]

Lists all tracks in the system. This outputs one line per track, plus final
summary information.

For each track, the following information is shown:

* The track's ID
* The size of the track's data (this includes the size of the data directory,
  but not the size of the database elements)
* The track's owner (both name and email address)
* The track's title.

REMOVING A TRACK
################

.. code-block:: text

    fietsctl track del [-c CONFIG] -i/--id ID [-f/--force]

Deletes the specified track. The right ID can be found via the ``track list``
command, or via the web interface.

This command deletes the track including its pictures and comments.

By default, the command will ask for confirmation. You can specify the
``-f``/``--force`` flag to override this check.

MAINTENANCE MODE
****************

The ``fietsctl maintenance-mode`` subcommand manages the maintenance mode.

ACTIVATING MAINTENANCE
######################

.. code-block:: text

    fietsctl maintenance-mode [-c CONFIG] REASON

Enables the maintenance mode with the given reason. The reason should be a
short string that describes why Fietsboek is disabled. It will be shown to the
users on the error page.

CHECKING MAINTENANCE
####################

.. code-block:: text

    fietsctl maintenance-mode [-c CONFIG]

Without the reason, the command will output the current status of the
maintenance mode.

DEACTIVATING MAINTENANCE
########################

.. code-block:: text

    fietsctl maintenance-mode [-c CONFIG] --disable

With the ``--disable`` option, the maintenance mode will be disabled.

BUGS
****

Bugs can be reported either via the issue tracker
(https://gitlab.com/dunj3/fietsboek/-/issues) or via email (fietsboek at
kingdread dot de).

AUTHOR
******

This program is part of Fietsboek, written by the Fietsboek authors.

COPYRIGHT
*********

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
